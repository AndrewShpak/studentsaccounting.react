import React from "react";
import Icon from "antd/es/icon";
import {useField} from "formik";
import Select from "antd/es/select";

const Option = Select.Option;

function FormSelect({label, setFieldValue, options, ...props}) {
    const [, meta] = useField(props.name);

    return (
        <div className={"field"}>
            <Select
                style={{width: '100%'}}
                placeholder="Please select"
                {...props}
                onChange={(value) => {
                    setFieldValue(props.name, value)
                }}
            >
                {getOptions(options)}
            </Select>
            <label>{label}</label>
            {meta.error ? (
                <div className="error">
                    <Icon type="info-circle"/>
                    <span>{meta.error}</span>
                </div>
            ) : null}
        </div>

    )
}

const getOptions = (items) => {
    const roles = [];
    items.forEach(item => {
        roles.push(<Option key={item.id}>{item.name}</Option>);
    });
    return roles;
};
export default FormSelect;
