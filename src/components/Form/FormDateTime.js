import React from "react";
import DatePicker from "antd/es/date-picker";
import moment from "moment";
import Icon from "antd/es/icon";
import {useField} from "formik";

function FormDateTime({label,setFieldValue,...props}) {
    const [, meta] = useField(props.name);
    if (props.value){
        props.value= moment(props.value);
    }else{
        delete props["value"];
    }

    return (
        <div className={"field"}>
            <DatePicker
                {...props}
                onChange={(value,dateString) => {
                    setFieldValue(props.name,  dateString)
                }}
            />
            <label>{label}</label>
            {meta.error ? (
                <div className="error">
                    <Icon type="info-circle" />
                    <span>{meta.error}</span>
                </div>
            ) : null}
        </div>

    )
}

export default FormDateTime;
