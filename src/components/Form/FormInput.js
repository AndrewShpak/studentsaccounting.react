import React from "react";
import {useField} from "formik";
import Icon from "antd/es/icon";

function FormInput({label, ...props}) {
    const [field, meta] = useField(props.name);
    return (
        <div className={"field"}>
            <input {...field} {...props}/>
            <label>{label}</label>
            {meta.touched && meta.error ? (
                <div className="error">
                    <Icon type="info-circle"/>
                    <span>{meta.error}</span>
                </div>
            ) : null}
        </div>
    )
}

export default FormInput;
FormInput.defaultProps = {
    value: "",
};
