import Modal from "antd/es/modal";
import React from "react";
import PropTypes from "prop-types";

function AppModal(props) {
    const {setVisible,...rest} = props;
    return (
        <Modal
            onOk={() => {
                setVisible(false)
            }}
            onCancel={() => setVisible(false)}
            {...rest}
        >
            {props.children}
        </Modal>

    );
}
export default AppModal;

AppModal.propTypes = {
    children: PropTypes.node.isRequired,
    visible: PropTypes.bool.isRequired,
    setVisible: PropTypes.func.isRequired,
    title: PropTypes.node.isRequired,
};
