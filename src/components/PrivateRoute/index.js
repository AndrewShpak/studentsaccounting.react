import {Route, withRouter} from "react-router-dom";
import React, {useEffect} from "react";
import Layout from "../AppLayout";
import PropTypes from "prop-types";
import notification from "antd/es/notification";
import Cookies from "js-cookie"

function PrivateRoute({component: Component, hasLayout, history, location, ...rest}) {
    const token = Cookies.get("sa_token");
    useEffect(() => {
        if (token === undefined && location.pathname !== "/") {
            history.push("/");
            notification["error"]({
                message: 'Error!',
                description: "You aren't logged in system",
            });
        }
    }, []);

    return (
        <Route
            {...rest}
            render={props => {
                return hasLayout
                    ?
                    <Layout>
                        <Component {...props} />
                    </Layout> :
                    <Component {...props} />;
            }
            }
        />
    )

}

export default withRouter(PrivateRoute);

PrivateRoute.propTypes = {
    hasLayout: PropTypes.bool
};
PrivateRoute.defaultProps = {
    hasLayout: true
};
