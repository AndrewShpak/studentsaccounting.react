import {Link} from "react-router-dom";
import React from "react";
import Menu from "antd/es/menu";
function AppMenu({logout}) {
    return (
        <Menu>
            <Menu.Item key="1">
                <Link to={"/profile"}>Profile</Link>
            </Menu.Item>
            <Menu.Item key="2" onClick={logout}>Logout</Menu.Item>
        </Menu>
    )
}

export default AppMenu;

