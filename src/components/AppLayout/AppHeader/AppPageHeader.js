import PageHeader from "antd/es/page-header";
import React from "react";
import {withRouter} from "react-router-dom";

function AppPageHeader({location}) {
    return (<>
        <PageHeader
            title={routesNameMap[location.pathname]}
        />
    </>)
}

export default withRouter(AppPageHeader);
const routesNameMap = {
    '/': 'Home',
    '/students': 'Students',
    '/courses': 'Courses',
    '/profile': 'Profile',
};
