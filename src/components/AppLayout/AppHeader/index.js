import React, {useCallback} from "react";
import "./styles.css";
import AppMenu from "./AppMenu";
import Dropdown from "antd/es/dropdown";
import Icon from "antd/es/icon";
import {useSelector} from "react-redux";
import Cookies from "js-cookie";
import notification from "antd/es/notification";
import {withRouter} from "react-router-dom";

function AppHeader({history}) {
    const user = useSelector(state => state.user, []);
    const logout = useCallback(
        () => {
            notification["info"]({
                message: 'Info!',
                description: "You logout!",
            });
            history.push("/");
            Cookies.remove("sa_token");
        }, []);
    return (
        <div className={"anticon header"}>
            <Dropdown overlay={<AppMenu logout={logout}/>}>
                <div>
                    <img
                        src={user.avatar}
                        alt={"user avatar"}/>
                    <span>{user.userName}</span>
                    <Icon type="down" style={{fontSize: "1.5em"}}/>
                </div>
            </Dropdown>
        </div>
    )

}

export default withRouter(AppHeader);
