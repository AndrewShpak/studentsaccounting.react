import React from"react"
import PropTypes from "prop-types";
function Main(props) {
    return(<main className="main-content">{props.children}</main>);
}
export default Main;

Main.propTypes = {
    children: PropTypes.node.isRequired
};
