import React, {useCallback, useEffect} from "react";
import {Icon, Menu} from "antd";
import {withRouter} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import axios from "axios";
import {getUserData} from "../../../pages/UserProfile/actions";
import Cookies from "js-cookie"

function Sidebar({history, location}) {
    const token = Cookies.get("sa_token");
    const user = useSelector(state => state.user, []);
    const dispatch = useDispatch();
    const fetchUserData = useCallback(() => dispatch(getUserData()), []);
    useEffect(() => {
        axios.defaults.headers['Authorization'] = `Bearer ${token}`;
        if (token !== undefined) {
            fetchUserData();
        }
    }, []);
    return (
        <>
            <div className="logo"/>
            <Menu theme="dark" mode="inline" selectedKeys={[getSelectedKey(location)]}>
                {user.roles.includes("Admin") &&
                <Menu.Item key="2" onClick={() => history.push("/students")}>
                    <Icon type="user"/>
                    <span>Студенти</span>
                </Menu.Item>}
                <Menu.Item key="3" onClick={() => history.push("/courses")}>
                    <Icon type="ordered-list"/>
                    <span>Курси</span>
                </Menu.Item>
            </Menu>
        </>
    );

}

export default withRouter(Sidebar);

function getSelectedKey(location) {
    switch (location.pathname) {
        case "/courses":
            return "3";
        case "/students":
            return "2";
        default:
            return "1";
    }
}
