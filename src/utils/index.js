import axios from "axios";
import Input from "antd/es/input";
import Button from "antd/es/button";
import React from "react";
import Icon from "antd/es/icon";

axios.defaults.baseURL = 'https://localhost:5001/api/v1';

export const updatedItems = (state, data) => {
    return state.map(item => {
        if (item.id === data.id) {
            return {...item, ...data}
        }
        return item;
    })
};

function FilterDropdown({
                            setSelectedKeys, selectedKeys, confirm, clearFilters,
                        }) {
    return (<div style={{padding: 8}}>
        <Input
            placeholder={`Search`}
            value={selectedKeys[0]}
            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            style={{width: 188, marginBottom: 8, display: 'block'}}
        />
        <Button
            type="primary"
            onClick={confirm}
            icon="search"
            size="small"
            style={{width: 90, marginRight: 8}}
        >
            Search
        </Button>
        <Button
            onClick={clearFilters}
            size="small"
            style={{width: 90}}
        >
            Clear
        </Button>
    </div>)
}

export const getColumnSearchProps = () => ({
    filterDropdown: (filter) => {
        return (<FilterDropdown {...filter}/>)
    },
    filterIcon: filtered => <Icon type="search" style={{color: filtered ? '#1890ff' : undefined}}/>,
});
export const onFilter = (dataIndex, value, record) => record[dataIndex].toString().toLowerCase().includes(value.toLowerCase());


