import React from 'react';
import Login from "./pages/Login";
import Students from "./pages/Students";
import Courses from "./pages/Courses";
import UserProfile from "./pages/UserProfile";
import SelectStudyDate from "./pages/SelectStudyDate";
import {BrowserRouter as Router, Route} from "react-router-dom";
import PrivateRoute from "./components/PrivateRoute";
import './App.css';
// import logo from "./logo.svg";
function App() {
    return (
        <Router>
            <div>
                <Route exact path="/" component={Login}/>
                <PrivateRoute path="/study-date" hasLayout={false} component={SelectStudyDate}/>
                <PrivateRoute path="/students" component={Students}/>
                <PrivateRoute path="/profile" component={UserProfile}/>
                <PrivateRoute path="/courses" component={Courses}/>
            </div>
        </Router>
    );
}

export default App;
