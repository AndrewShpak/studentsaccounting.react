import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import "./utils";
import {store} from "./store"
import 'antd/dist/antd.min.css';
import './index.css';
import {Provider} from "react-redux";

ReactDOM.render(
        <Provider store={store}>
            <App/>
        </Provider>
    , document.getElementById('root'));

serviceWorker.unregister();
