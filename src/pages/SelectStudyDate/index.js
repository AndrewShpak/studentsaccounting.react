import React, {useCallback} from "react";
import "./styles.css"
import Card from "antd/es/card";
import Icon from "antd/es/icon";
import {submitForm} from "./actions";
import {Form, Formik} from "formik";
import StudyDateFormSchema from "./StudyDateFormSchema";
import FormDateTime from "../../components/Form/FormDateTime";
import Button from "antd/es/button";
import moment from "moment";
import {withRouter} from "react-router-dom";

function SelectStudyDate({history}) {
    const onSubmit = useCallback(
        ({studyDate}, actions) => {
            submitForm(moment(studyDate).format());
            history.push("/courses")
        },
        []
    );
    return (
        <div className={"study-date"}>
            <Card
                title={<div><Icon type="calendar"/> Enter Study Date</div>}
            >
                <Formik
                    initialValues={{studyDate: moment()}}
                    onSubmit={onSubmit}
                    validateOnChange
                    validationSchema={StudyDateFormSchema}
                    render={({handleSubmit, setFieldValue, values}) => (
                        <Form onSubmit={handleSubmit}>
                            <FormDateTime
                                allowClear={false}
                                name="studyDate"
                                setFieldValue={setFieldValue}
                                label={"Study Date"}
                                value={values.studyDate}
                                placeholder={"Study Date"}
                            />

                            <div className={"submit"}>
                                <Button type="primary" htmlType={"submit"}>Submit</Button>
                            </div>
                        </Form>
                    )}
                />
            </Card>
        </div>

    )
}

export default withRouter(SelectStudyDate);
