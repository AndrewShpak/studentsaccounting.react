import * as Yup from "yup";

const StudyDateFormSchema = Yup.object().shape({
    studyDate: Yup.date().min(new Date(),"Data must be greater than now!").required('Required'),
});
export default StudyDateFormSchema;
