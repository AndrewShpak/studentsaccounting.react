import axios from "axios";
import notification from "antd/es/notification";

export const submitForm = (date) => {
    const request = axios.post(`/study-year`, {date});
    return (dispatch)=> {
        request.then(() => {
            notification["success"]({
                message: "Success!",
                description: "Form Submitted"
            });
        })
            .catch(() => {
                notification["warning"]({
                    message: "Error!",
                    description: "Submitting form"
                });
            });
    }
};
