import React, {useCallback, useEffect} from "react";
import Table from "antd/es/table";
import {getColumnSearchProps, onFilter} from "../../utils";
import {useDispatch, useSelector} from "react-redux";
import {fetchCourses} from "./actions";
import AddCourse from "./components/AddCourse";
import DeleteConfirm from "./components/DeleteConfirm";
import EditCourse from "./components/EditCourse";
import RegisterIcon from "./components/RegisterIcon";

function Courses() {
    const dispatch = useDispatch();
    const fetch = useCallback(() => dispatch(fetchCourses()), []);
    useEffect(() => {
        fetch()
    }, []);
    const coursesList = useSelector(state => state.courses, []);
    return (
        <div className={"courses"}>
            <Table
                columns={columns}
                rowKey={record => record.id}
                dataSource={coursesList}
                bordered
                size="middle"
                onChange={(pagination, filters, sorter) => {

                }}
            />
        </div>
    )
}

export default Courses;

const columns = [{
    title: 'Name',
    dataIndex: 'name',
    filtered: true,
    align: "center",
    sorter: (a, b) => a.name.length - b.name.length,
    onFilter: (value, record) => onFilter("name", value, record),
    ...getColumnSearchProps(),
}, {
    title: 'ShortName',
    dataIndex: 'shortName',
    filtered: true,
    align: "center",
    sorter: (a, b) => a.shortName.length - b.shortName.length,
    onFilter: (value, record) => onFilter("shortName", value, record),
    ...getColumnSearchProps(),
},
    {

        dataIndex: 'actions',
        width: '15%',
        filtered: true,
        align: "center",
        title: () => <AddCourse buttonTitle={"Add"} modalTitle={"Add Course"} iconType={"plus-circle"}/>,
        render: (text, record) => {
            return (
                <>
                    <RegisterIcon id={record.id} isRegistered={record.isRegistered}/>
                    {record.editEnabled && <EditCourse record={record} modalTitle={"Edit Course"} iconType={"form"}/>}
                    {record.deleteEnabled && <DeleteConfirm id={record.id}/>}
                </>
            )
        }
    }
];

