import {CREATE_COURSE, DELETE_COURSE, FETCH_COURSES, TOGGLE_COURSE_SUBSCRIPTION, UPDATE_COURSE} from "./types";
import {updatedItems} from "../../utils";

const initialState = [];

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_COURSES:
            return [
                ...action.payload
            ];
        case UPDATE_COURSE:
            return updatedItems(state, action.payload);
        case CREATE_COURSE:
            return state.concat(action.payload);
        case DELETE_COURSE:
            return state.filter(i => i.id !== action.payload);
        case TOGGLE_COURSE_SUBSCRIPTION:
           const a=  toggleCourseSubscription(state, action.payload);
            return a;
        default:
            return state;
    }
}
const toggleCourseSubscription = (state, id) => {
    return state.map(item => {
        if (item.id === id) {
            item.isRegistered = !item.isRegistered;
            return {...item}
        }
        return item;
    })
};
