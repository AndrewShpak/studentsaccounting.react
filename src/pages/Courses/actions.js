import axios from "axios";
import notification from "antd/es/notification";
import {CREATE_COURSE, DELETE_COURSE, FETCH_COURSES, TOGGLE_COURSE_SUBSCRIPTION, UPDATE_COURSE} from "./types";

export const fetchCourses = () => {
    const request = axios.get(`/courses`);
    return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: FETCH_COURSES, payload: data})
        })
            .catch(() => {
                notification["error"]({
                    message: 'Error!',
                    description: "When you fetch courses an error.",
                })
            });
    }
};
export const updateCourseInfo = (course) => {
    const request = axios.put(`/course/edit`, course);
    return (dispatch) => {
        request.then(() => {
            dispatch({type: UPDATE_COURSE, payload: course});
            notification["success"]({
                message: "Success!",
                description: "Form Submit"
            });
        })
            .catch(() => {
                notification["error"]({
                    message: "Error!",
                    description: "Submitting form"
                });
            });
    }
};
export const createCourse = (course) => {
    const request = axios.post(`/course/create`, course);
    return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: CREATE_COURSE, payload: data});
            notification["success"]({
                message: "Success!",
                description: "Form Submit"
            });
        })
            .catch((error) => {
                notification["error"]({
                    message: "Error!",
                    description: "Submitting form"
                });
            });
    }
};
export const deleteCourse = (id) => {
    const request = axios.delete(`/course/delete`, {params: {id}});
    return (dispatch) =>
        request.then(() => {
            dispatch({type: DELETE_COURSE, payload: id});
            notification["success"]({
                message: "Success!",
                description: "Course Deleted"
            });
        }).catch((error) => {
                notification["error"]({
                    message: "Error!",
                    description: "Delete Course"
                });
            });
};
export const subscribeToCourse = (id) => {
    const request = axios.post(`/course/subscribe`, {id});
    return (dispatch) => {
        request.then(() => {
            dispatch({type: TOGGLE_COURSE_SUBSCRIPTION, payload: id});
            notification["success"]({
                message: "Success!",
                description: ""
            });
        })
            .catch(() => {
                notification["error"]({
                    message: "Error!",
                    description: "Form Submit"
                });
            });
    }
};

