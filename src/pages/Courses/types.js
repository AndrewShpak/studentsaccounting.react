export const FETCH_COURSES = "[Courses] Fetch Courses";
export const UPDATE_COURSE = "[Courses] Update Course Info";
export const CREATE_COURSE = "[Courses] Create Course";
export const DELETE_COURSE = "[Courses] Delete Course";
export const TOGGLE_COURSE_SUBSCRIPTION = "[Courses] Toggle Course Subscription";
