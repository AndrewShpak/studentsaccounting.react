import {Form, Formik} from "formik";
import FormInput from "../../../components/Form/FormInput";
import Button from "antd/es/button";
import React, {useCallback} from "react";
import {useDispatch} from "react-redux";
import * as PropTypes from "prop-types";
import CourseSchema from "./CourseSchema";
import {createCourse, updateCourseInfo} from "../actions";

function CourseForm({record, setVisible}) {
    const dispatch = useDispatch();
    const onSubmit = useCallback((values,actions) => {
        if (values.id) dispatch(updateCourseInfo(values));
        else dispatch(createCourse(values));
        setVisible(false);
        actions.resetForm()
    }, []);
    return (
        <Formik
            initialValues={record}
            onSubmit={onSubmit}
            validationSchema={CourseSchema}
            render={({handleSubmit, values}) => (
                <Form onSubmit={handleSubmit}>
                    <FormInput name="name" type="text" label="Name" value={values.name}
                               placeholder={"Enter Course Name"}/>
                    <FormInput name="shortName" type="text" label="ShortName" value={values.shortName}
                               placeholder={"Enter Course ShortName"}/>
                    <div className={"submit"}>
                        <Button onClick={() => setVisible(false)}>Cancel</Button>
                        <Button type="primary" htmlType={"submit"}>Submit</Button>
                    </div>
                </Form>
            )}
        />
    )
}

export default CourseForm;
CourseForm.propTypes = {
    record: PropTypes.object,
    setVisible: PropTypes.func.isRequired
};
CourseForm.defaultProps = {
    record: {},
};
