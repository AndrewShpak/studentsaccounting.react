import Icon from "antd/es/icon";
import React, {useCallback} from "react";
import PropTypes from "prop-types";
import Tooltip from "antd/es/tooltip";
import Modal from "antd/es/modal";
import {useDispatch} from "react-redux";
import {subscribeToCourse} from "../actions";

const confirm = Modal.confirm;

function RegisterIcon({isRegistered, id}) {
    const dispatch = useDispatch();
    const unregister = useCallback(() => {
        confirm({
            title: 'Do you want unregister on this course?',
            onOk() {
                dispatch(subscribeToCourse(id))
            },
            onCancel() {
            },
        });
    }, []);
    const register = useCallback(() => {
        confirm({
            title: 'Do you want register on this course?',
            onOk() {
                dispatch(subscribeToCourse(id))
            },
            onCancel() {
            },
        });
    }, []);
    return isRegistered ? (
        <Tooltip title="Unregister">
            <Icon type="close-square" onClick={unregister} className={"green"}
                  style={{fontSize: "1.5em"}}/>
        </Tooltip>
    ) : (
        <Tooltip title="Register">
            <Icon type="check-square" onClick={register} style={{fontSize: "1.5em"}}/>
        </Tooltip>

    );

}

export default RegisterIcon;


RegisterIcon.propTypes = {
    isRegistered: PropTypes.bool.isRequired,
    id: PropTypes.number.isRequired,
};
RegisterIcon.defaultProps = {
    isRegistered: false
};
