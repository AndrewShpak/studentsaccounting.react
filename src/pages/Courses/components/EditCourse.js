import React, {useState} from "react";
import CourseForm from "./CourseForm";
import AppModal from "../../../components/Modal";
import PropTypes from "prop-types";
import Tooltip from "antd/es/tooltip";
import Icon from "antd/es/icon";

function EditCourse({modalTitle, iconType, record}) {
    const [visible, setVisible] = useState(false);
    return (
        <>
            <Tooltip title="Edit">
                <Icon className={"blue ml-1"} type={"form"} onClick={() => setVisible(true)}
                      style={{fontSize: "1.5em"}}/>
            </Tooltip>
            <AppModal visible={visible} setVisible={setVisible}
                      footer={null} title={
                <>
                    <Icon type={iconType}/> {modalTitle}
                </>
            }>
                <CourseForm setVisible={setVisible} record={record}/>
            </AppModal>
        </>
    );
}

export default EditCourse;
EditCourse.propTypes = {
    modalTitle: PropTypes.string.isRequired,
    iconType: PropTypes.string.isRequired,
    record: PropTypes.object
};
EditCourse.defaultProps = {
    record: {}
};
