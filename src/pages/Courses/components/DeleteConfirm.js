import Tooltip from "antd/es/tooltip";
import React, {useCallback} from "react";
import Icon from "antd/es/icon";
import Modal from "antd/es/modal";
import {useDispatch} from "react-redux";
import {deleteCourse} from "../actions";
import PropTypes from "prop-types";

const confirm = Modal.confirm;

function DeleteConfirm({id}) {
    const dispatch = useDispatch();
    const onOk = useCallback(() => dispatch(deleteCourse(id)), []);
    return (
        <Tooltip title="Delete">
            <Icon className={"red ml-1"} onClick={() =>
                confirm({
                    title: 'Do you want to delete these course?',
                    onOk,
                    onCancel() {
                    },
                })} type="delete" style={{fontSize: "1.5em"}}/>
        </Tooltip>
    );
}


DeleteConfirm.propTypes = {
    id: PropTypes.number.isRequired,
};
export default DeleteConfirm;
