import * as Yup from "yup";

const CourseSchema = Yup.object().shape({
    name: Yup
        .string()
        .min(2, 'Too Short!')
        .max(200, 'Too Long!')
        .required('Required'),
    shortName: Yup
        .string()
        .min(2, 'Too Short!')
        .max(200, 'Too Long!')
        .required('Required'),
});
export default CourseSchema;
