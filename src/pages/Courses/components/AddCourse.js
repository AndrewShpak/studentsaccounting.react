import React, {useState} from "react";
import {Button} from "antd";
import CourseForm from "./CourseForm";
import AppModal from "../../../components/Modal";
import Icon from "antd/es/icon";
import PropTypes from "prop-types";

function AddCourse({buttonTitle, modalTitle, iconType}) {
    const [visible, setVisible] = useState(false);
    return (
        <>
            <Button type={"primary"} icon={"plus-circle"} onClick={() => setVisible(true)}>{buttonTitle}</Button>
            <AppModal visible={visible} setVisible={setVisible}
                      footer={null} title={
                <>
                    <Icon type={iconType}/> {modalTitle}
                </>
            }>
                <CourseForm setVisible={setVisible}/>
            </AppModal>
        </>
    );
}

export default AddCourse;
AddCourse.propTypes = {
    buttonTitle: PropTypes.string.isRequired,
    modalTitle: PropTypes.string.isRequired,
    iconType: PropTypes.string.isRequired,
};
