import React, {useCallback, useEffect, useMemo} from 'react';
import "./styles.css"
import Card from "antd/es/card";
import {withRouter} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {signIn} from "./actions";
import FacebookLogin from "react-facebook-login";
import Cookies from "js-cookie"

function Login({history}) {
    const token = Cookies.get("sa_token");
    const dispatch = useDispatch();
    const isAuthenticated = useSelector(state => state.isAuthenticated, []);
    const user = useSelector(state => state.user, [isAuthenticated]);
    const login = useCallback(
        (response) => dispatch(signIn(response)),
        []
    );
    useEffect(() => {
        if (token !== undefined && isAuthenticated && user.id.length > 0) {
            if (user.hasStudyDate) history.push("/courses");
            else if (!user.hasStudyDate) history.push("/study-date")
        }
    }, [isAuthenticated,user]);

    return useMemo(() => {
        return (
            <div className={"login-form"}>
                <Card
                    title={<div>
                        <img src={process.env.PUBLIC_URL + `/logo.ico`} height={50}
                             alt={"Students accounting system logo"}/>
                        <strong> Students Accounting System</strong>
                    </div>}
                >
                    <FacebookLogin
                        appId="488888938518784"
                        fields="name,email,picture"
                        callback={login}
                        icon="fa-facebook"
                    />
                </Card>
            </div>
        )
    }, []);

}

export default withRouter(Login);
