import axios from "axios";
import {LOGIN} from "./types";
import notification from "antd/es/notification";
import Cookies from "js-cookie";
import {getUserData} from "../UserProfile/actions";

export const signIn = (response) => {
    const request = axios.post(`/login`, {
        userName: response.name,
        email: response.email,
        avatar: response.picture.data.url
    });
    return (dispatch) => {
        request.then(({data}) => {
            axios.defaults.headers['Authorization'] = `Bearer ${data}`;
            Cookies.set('sa_token', data, {expires: .8});
            dispatch({type: LOGIN})

        }).then(() => {
            dispatch(getUserData())
        })
            .then(() => {

            }).catch(() => {

            notification["error"]({
                message: 'Error!',
                description: "When you log an error",
            });
        });
    }
};
