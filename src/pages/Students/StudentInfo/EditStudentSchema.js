import * as Yup from "yup";

const EditStudentSchema = Yup.object().shape({
    name: Yup
        .string()
        .min(2, 'Too Short!')
        .max(200, 'Too Long!')
        .required('Required'),
    lastName: Yup
        .string()
        .min(2, 'Too Short!')
        .max(200, 'Too Long!')
        .required('Required'),
    email: Yup
        .string()
        .email('Invalid email')
        .required('Required'),
    age: Yup
        .number()
        .positive("Need enter positive Age")
        .integer("Need enter integer")
        .required('Required'),
    studyDate: Yup.date(),
});
export default EditStudentSchema;
