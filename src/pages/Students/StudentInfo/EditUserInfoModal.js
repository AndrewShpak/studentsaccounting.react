import React, {useCallback, useState} from "react";
import Button from "antd/es/button/index";
import Icon from "antd/es/icon/index";
import AppModal from "../../../components/Modal";
import {Form, Formik} from "formik";
import EditStudentSchema from "./EditStudentSchema";
import FormInput from "../../../components/Form/FormInput";
import FormDateTime from "../../../components/Form/FormDateTime";
import {useDispatch, useSelector} from "react-redux";
import {updateUserInfo} from "../actions";
import FormSelect from "../../../components/Form/FormSelect";

function EditUserInfoModal({record}) {
    const [visible, setVisible] = useState(false);
    const user = useSelector(state => state.user, []);
    const dispatch = useDispatch();
    const onSubmit = useCallback((values, actions) => {
        dispatch(updateUserInfo(values));
        setVisible(false);
    }, []);
    return (
        <>
            <Button type="primary" icon={"form"} onClick={() => {
                setVisible(true)
            }}>
                Edit
            </Button>
            <AppModal
                title={
                    <>
                        <Icon type="form"/> Edit Student Info
                    </>
                }
                visible={visible}
                setVisible={setVisible}
                footer={null}
            >
                <Formik
                    initialValues={record}
                    onSubmit={onSubmit}
                    validationSchema={EditStudentSchema}
                    render={({handleSubmit, setFieldValue, values}) => (
                        <Form onSubmit={handleSubmit}>
                            <FormInput name="id" type="text" label="Id" value={values.id} readOnly
                                       placeholder={"Student Id"}/>
                            <FormInput name="name" type="text" label="Name" value={values.name}
                                       placeholder={"Enter Student Name"}/>
                            <FormInput name="lastName" type="text" label="Last Name" value={values.lastName}
                                       placeholder={"Enter Student LastName"}/>
                            <FormInput name="age" type="number" label="Age" value={values.age}
                                       placeholder={"Enter Student Age"}/>
                            <FormInput name="email" type="email" label="Email" readOnly value={values.email}
                                       placeholder={"Enter Student Email"}/>
                            <FormDateTime
                                allowClear={false}
                                name="registered"
                                setFieldValue={setFieldValue}
                                disabled
                                label={"Registered Date"}
                                value={values.registered}
                                placeholder={"Student RegisteredDate"}
                            />
                            <FormDateTime
                                allowClear={false}
                                name="studyDate"
                                disabled
                                setFieldValue={setFieldValue}
                                label={"Study Date"}
                                value={values.studyDate}
                                placeholder={"Study Date"}
                            />
                             <FormSelect
                                mode="multiple"
                                allowClear={false}
                                name="roles"
                                defaultValue={values.roles}
                                setFieldValue={setFieldValue}
                                options={user.userRoles}
                                label={"User Roles"}
                            />
                            <div className={"submit"}>
                                <Button onClick={() => setVisible(false)}>Cancel</Button>
                                <Button type="primary" htmlType={"submit"}>Submit</Button>
                            </div>
                        </Form>
                    )}
                />
            </AppModal>
        </>
    );
}

export default EditUserInfoModal;


