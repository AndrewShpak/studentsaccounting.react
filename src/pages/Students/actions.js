import axios from "axios";
import {FETCH_STUDENTS, UPDATE_STUDENT} from "./types";
import notification from "antd/es/notification";

export const fetchStudents = (params = {}) => {
    const request = axios.get(`/students`, {params});
    return (dispatch)=> {
        request.then(({data}) => {
            dispatch({type: FETCH_STUDENTS, payload: data})
        })
            .catch(() => {
                notification["error"]({
                    message: 'Error!',
                    description: "When you fetch students an error.",
                })
            });
    }
};

export const updateUserInfo = (student) => {
    const request = axios.put(`/user/edit`, student);
    return (dispatch)=> {
        request.then(() => {
            dispatch({type: UPDATE_STUDENT, payload: student});
            notification["success"]({
                message: "Success!",
                description: "Form Submit"
            });
        })
            .catch(() => {
                notification["error"]({
                    message: "Error!",
                    description: "Submitting form"
                });
            });
    }
};


