import EditUserInfo from "./StudentInfo/EditUserInfoModal";
import React from "react";
import {getColumnSearchProps} from "../../utils";
const defaultColumnOptions = {
    sorter: true,
    filtered: true,
    align: "center",
    ...getColumnSearchProps()
};


export const columns = [
    {
        title: 'Id',
        dataIndex: 'id',
        key: 'id',
        ...defaultColumnOptions
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        ...defaultColumnOptions,
    },
    {
        title: 'Last Name',
        dataIndex: 'lastName',
        key: 'lastName',
        ...defaultColumnOptions,
    },
    {
        title: 'Age',
        dataIndex: 'age',
        key: 'age',
        ...defaultColumnOptions,
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
        ...defaultColumnOptions,
    },
    {
        title: 'Registered Date',
        dataIndex: 'registered',
        key: 'registered',
        ...defaultColumnOptions,
    },
    {
        title: 'Study Date',
        dataIndex: 'studyDate',
        key: 'studyDate',
        ...defaultColumnOptions,
    },
    {
        title: '',
        dataIndex: 'id',
        key:"actions",
        render: (text, record, index) => <EditUserInfo text={text} record={record} index={index}/>
    }
];
