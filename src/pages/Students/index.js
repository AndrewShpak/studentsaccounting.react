import React, {useCallback, useEffect, useMemo} from "react";
import Table from "antd/es/table";
import {fetchStudents} from "./actions";
import {useDispatch, useSelector} from "react-redux";
import {columns} from "./utils";


function Students() {
    const dispatch = useDispatch();
    const students = useSelector(state => state.students, []);
    const fetch = useCallback(() => dispatch(fetchStudents()), []);
    const onChange = useCallback((pagination, filters, sorter) => {
        const params = {
            current: pagination.current,
            sortField: sorter.field,
            sortOrder: sorter.order,
            filters,
        };
        dispatch(fetchStudents(params))
    }, []);
    useEffect(() => {
        fetch()
    }, []);

    return useMemo(() => {
        return (
            <div className={"students"}>
                <Table
                    columns={columns}
                    rowKey={record => record.id}
                    size="middle"
                    dataSource={students.data}
                    pagination={students.pagination}
                    loading={false}
                    bordered
                    onChange={onChange}
                />
            </div>
        )
    }, [students]);

}

export default Students;


