import {FETCH_STUDENTS, UPDATE_STUDENT} from "./types";
import {updatedItems} from "../../utils";

const initialState = {
    data: [],
    pagination: {}
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_STUDENTS:
            return {
                ...state,
                data: action.payload.students,
                pagination: action.payload.pagination,
                filters: action.payload.filters
            };
        case UPDATE_STUDENT:
            return {
                ...state,
                data:updatedItems(state.data,action.payload)
            };
        default:
            return state;
    }
}



