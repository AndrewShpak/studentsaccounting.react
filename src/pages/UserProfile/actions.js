import axios from "axios";
import {notification} from "antd/es";
import {FETCH_USER_DATA, UPDATE_USER} from "./types";

export function submitForm(values) {
    const request = axios.put(`/user/edit`, values);
    return (dispatch) => {
        request.then(() => {
            dispatch({type: UPDATE_USER, payload: values});
            notification["success"]({
                message: "Success!",
                description: "Form Submit"
            });
        })
            .catch(() => {
                notification["warning"]({
                    message: "Error!",
                    description: "Submitting form"
                });
            });
    }
}

export const getUserData = () => {
    const request = axios.get(`/userData`);
    return (dispatch) => {
        request.then(({data}) => {
            dispatch({type: FETCH_USER_DATA, payload: data});
        }).catch(() => {

        });
    }
};
