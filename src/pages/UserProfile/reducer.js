import {FETCH_USER_DATA, UPDATE_USER} from "./types";

const initialState = {
    userRoles: [],
    roles: [],
    avatar: "",
    userName: "",
    name:"",
    id:"",
    studyDate:"",
    lastName:"",
    email:"",
    age:0,
    hasStudyDate:false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case UPDATE_USER:
        case FETCH_USER_DATA:
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
}
