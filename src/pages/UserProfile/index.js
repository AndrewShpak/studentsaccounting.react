import React, {useCallback} from "react";
import FormDateTime from "../../components/Form/FormDateTime";
import FormInput from "../../components/Form/FormInput";
import {UserProfileSchema} from "./UserProfileSchema";
import {Form, Formik} from "formik";
import Button from "antd/es/button";
import {useDispatch, useSelector} from "react-redux";
import {submitForm} from "./actions";

function UserProfile() {
    const dispatch = useDispatch();
    const user = useSelector(state => state.user, []);
    const onSubmit = useCallback((values, actions) => {
        const {id,name,lastName,age} = values;
        dispatch(submitForm({
            id,name,lastName,age
        }));
    }, []);
    if (user.id.length === 0) return (<div>Loading</div>);
    return (
        <Formik
            initialValues={user}
            onSubmit={onSubmit}
            validationSchema={UserProfileSchema}
            render={({handleSubmit, setFieldValue, values}) => {
                return (<Form onSubmit={handleSubmit}>
                        <FormInput name="name" type="text" label="Name" value={values.name}
                                   placeholder={"Enter your name"}/>
                        <FormInput name="lastName" type="text" label="Last Name" value={values.lastName}
                                   placeholder={"Enter your last name"}/>
                        <FormInput name="age" type="number" label="Age" value={values.age}
                                   placeholder={"Enter your age"}/>
                        <FormInput name="email" type="email" label="Email" readOnly value={values.email}
                                   placeholder={"Your email"}/>
                        <FormDateTime
                            allowClear={false}
                            name="registered"
                            setFieldValue={setFieldValue}
                            disabled
                            label={"Registered Date"}
                            value={values.registered}
                            placeholder={"Student registered date"}
                        />
                        <FormDateTime
                            allowClear={false}
                            name="studyDate"
                            disabled
                            setFieldValue={setFieldValue}
                            label={"Study Date"}
                            value={values.studyDate}
                            placeholder={"Study Date"}
                        />
                        <div className={"submit"}>
                            <Button>Cancel</Button>
                            <Button type="primary" htmlType={"submit"}>Submit</Button>
                        </div>
                    </Form>
                )
            }}
        />
    )
}

export default UserProfile;
