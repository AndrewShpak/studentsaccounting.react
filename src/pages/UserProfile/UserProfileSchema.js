import * as Yup from "yup";

export const UserProfileSchema = Yup.object().shape({
    name: Yup
        .string()
        .min(2, 'Too Short!')
        .max(200, 'Too Long!')
        .required('Required'),
    lastName: Yup
        .string()
        .min(2, 'Too Short!')
        .max(200, 'Too Long!')
        .required('Required'),
    email: Yup
        .string()
        .email('Invalid Email'),
    age: Yup
        .number()
        .positive("Need enter positive age")
        .integer("Need enter integer")
        .min(2,"Min number 2")
        .required('Required'),
    studyDate: Yup.date(),
});
