import {combineReducers} from "redux";
import isAuthenticated from "../pages/Login/reducer"
import students from "../pages/Students/reducer"
import courses from "../pages/Courses/reducer"
import user from "../pages/UserProfile/reducer"
const rootReducer=combineReducers({
    isAuthenticated,
    students,
    courses,
    user
});
export default rootReducer;
